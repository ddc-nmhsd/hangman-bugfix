package edu.cnm.deepdive.hangman;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

class GameTest {

  static final int STOCHASTIC_TEST_REPETITIONS = 10;
  
  Game game;

  @BeforeEach
  void createGame() {
    game = new Game();
  }

 @Test
  void constructor() {
    assertTrue(game.getWordList() != null && !game.getWordList().isEmpty());
  } 

 @Test
 void constructor_numGuesses() {
   Game game = new Game(50);
   game.newWord();
   assertEquals(50,game.getGuessesLeft());
 } 

  @Test
  void constructor_alternateWords() {
    Game game = new Game("test-words.txt");
    assertTrue(game.getWordList() != null && game.getWordList().size() == 1);
    game.newWord();
    assertEquals("singleton", game.getCurrentWord());
  }
  
  @Test
  void newWord() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    assertAll(
        () -> assertTrue(game.getGuessesLeft() > 0), () -> assertNotNull(currentWord),
        () -> assertFalse(currentWord.isBlank()),
        () -> assertTrue(game.getWordList().contains(currentWord)),
        () -> assertTrue(game.getGuessedLetters().isEmpty()),
        () -> assertEquals(currentWord.length(), game.getDisplayBuilder().length()),
        () -> assertTrue(game.getDisplayBuilder().toString().matches("^_+$"))
    );
  }

  @RepeatedTest(STOCHASTIC_TEST_REPETITIONS)
  void guess_correct() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    char guess = currentWord.charAt(new Random().nextInt(currentWord.length()));
    String other = "[^" + guess + "]";
    int guessesLeft = game.getGuessesLeft();
    Set<Character> guessedLetters = new HashSet<>(game.getGuessedLetters());
    String displayBefore = game.getDisplayBuilder().toString();
    game.guess(guess);
    assertAll(
        () -> assertEquals(guessesLeft, game.getGuessesLeft()),
        () -> assertFalse(guessedLetters.contains(guess)),
        () -> assertTrue(game.getGuessedLetters().contains(guess)),
        () -> assertEquals(-1, displayBefore.indexOf(guess)), 
        () -> assertTrue(game.getCurrentWord()
            .matches(game.getDisplayBuilder().toString().replaceAll("_", other)))
    );
  }

  @RepeatedTest(STOCHASTIC_TEST_REPETITIONS)
  void guess_incorrect() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    String incorrectLetters = IntStream
        .rangeClosed('a', 'z')
        .filter((c) -> currentWord.indexOf(c) < 0)
        .mapToObj((v) -> Character.valueOf((char) v))
        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
        .toString();
    int guessIndex = new Random().nextInt(incorrectLetters.length());
    char guess = incorrectLetters.charAt(guessIndex);
    int guessesLeft = game.getGuessesLeft();
    Set<Character> guessedLetters = new HashSet<>(game.getGuessedLetters());
    String displayBefore = game.getDisplayBuilder().toString();
    game.guess(guess);
    assertAll(
        () -> assertEquals(guessesLeft - 1, game.getGuessesLeft()),
        () -> assertFalse(guessedLetters.contains(guess)),
        () -> assertTrue(game.getGuessedLetters().contains(guess)),
        () -> assertEquals(-1, displayBefore.indexOf(guess)),
        () -> assertEquals(displayBefore, game.getDisplayBuilder().toString())
    );
  }

  @RepeatedTest(STOCHASTIC_TEST_REPETITIONS)
  void guess_repeat() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    String allLetters = IntStream
        .rangeClosed('a', 'z')
        .mapToObj((v) -> Character.valueOf((char) v))
        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
        .toString();
    int guessIndex = new Random().nextInt(allLetters.length());
    char guess1 = allLetters.charAt(guessIndex);
    char guess2 = allLetters.charAt((guessIndex + 1) % allLetters.length());
    game.guess(guess1);
    game.guess(guess2);
    int guessesLeft = game.getGuessesLeft();
    Set<Character> guessedLetters = new HashSet<>(game.getGuessedLetters());
    String displayBefore = game.getDisplayBuilder().toString();
    game.guess(guess1);
    assertAll(
        () -> assertEquals(guessesLeft, game.getGuessesLeft()),
        () -> assertTrue(guessedLetters.contains(guess1)),
        () -> assertEquals(guessedLetters, game.getGuessedLetters()),
        () -> assertEquals(displayBefore, game.getDisplayBuilder().toString())
    );
  }

  @RepeatedTest(STOCHASTIC_TEST_REPETITIONS)
  void isOver_guessed() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    List<Character> correctLetters = currentWord
        .codePoints()
        .distinct()
        .mapToObj((v) -> Character.valueOf((char) v))
        .collect(Collectors.toCollection(ArrayList::new));
    Collections.shuffle(correctLetters);
    correctLetters.forEach((c) -> {
      assertFalse(game.isOver());
      game.guess(c);
    });
    assertTrue(game.isOver());
  }

  @RepeatedTest(STOCHASTIC_TEST_REPETITIONS)
  void isOver_notGuessed() {
    game.newWord();
    String currentWord = game.getCurrentWord();
    List<Character> incorrectLetters = IntStream
        .rangeClosed('a', 'z')
        .filter((c) -> currentWord.indexOf(c) < 0)
        .mapToObj((v) -> Character.valueOf((char) v))
        .collect(Collectors.toCollection(ArrayList::new));
    Collections.shuffle(incorrectLetters);
    for (char c : incorrectLetters) {
      assertFalse(game.isOver());
      game.guess(c);
      if (game.getGuessesLeft() == 0) {
        break;
      }
    }
    assertTrue(game.isOver());
  }

}
