package edu.cnm.deepdive.hangman;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class MainTest {

  @ParameterizedTest
  @ValueSource(strings = {"\n","y","Y","yes","Yes","ok"} )
  void playAgain_continue(String input) {
    InputStream stream = new ByteArrayInputStream(input.getBytes());
    Scanner scanner = new Scanner(stream);
    assertTrue(Main.playAgain(scanner));
  }
 
  @ParameterizedTest
  @ValueSource(strings = {"n","N","no","No","Nah"} )
  void playAgain_stop(String input) {
    InputStream stream = new ByteArrayInputStream(input.getBytes());
    Scanner scanner = new Scanner(stream);
    assertFalse(Main.playAgain(scanner));
  }
  
}
